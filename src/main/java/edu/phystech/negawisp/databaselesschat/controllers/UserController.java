package edu.phystech.negawisp.databaselesschat.controllers;

import edu.phystech.negawisp.databaselesschat.dataasset.dto.PostDTO;
import edu.phystech.negawisp.databaselesschat.service.ThreadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/@ch")
public class UserController {
    long messagesAmount   = 19327212;
    private Logger logger = LoggerFactory.getLogger(UserController.class);
    private final ThreadService threadService;

    public UserController(ThreadService threadService) {
        this.threadService = threadService;
    }

    @RequestMapping(method=RequestMethod.POST, value="/{thread}")
    public ResponseEntity addMessage (
            @RequestBody PostDTO postDTO,
            @PathVariable String thread
            ) {
        logger.info("New post at a thread {}:", thread);
        return threadService.addMessage(thread, ++messagesAmount, postDTO);
    }

    @RequestMapping(method=RequestMethod.GET, value="/{thread}")
    public ResponseEntity<String> getChat (@PathVariable String thread) {
        logger.info("UserController.getChat");
        return threadService.getThreadAsString(thread);
    }
}
