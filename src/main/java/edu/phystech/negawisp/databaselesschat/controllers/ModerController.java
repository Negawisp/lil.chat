package edu.phystech.negawisp.databaselesschat.controllers;

import edu.phystech.negawisp.databaselesschat.dataasset.dto.PostRemovalDTO;
import edu.phystech.negawisp.databaselesschat.service.ModerThreadService;
import edu.phystech.negawisp.databaselesschat.service.ThreadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/@ch/moderator")
public class ModerController {
    private Logger logger = LoggerFactory.getLogger(ModerController.class);
    private final ModerThreadService moderService;

    public ModerController(ModerThreadService moderService) {
        this.moderService = moderService;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/{thread}")
    public ResponseEntity deletePost (
            @PathVariable String thread,
            @RequestBody PostRemovalDTO postRemovalDTO
            ) {
        logger.info("Deleting post {} at a thread {}.", postRemovalDTO.getPostID(), thread);
        return moderService.deleteMessage(thread, postRemovalDTO);
    }
}
