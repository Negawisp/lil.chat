package edu.phystech.negawisp.databaselesschat.storage.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PostPOJO {
    private String userName;
    private String sendingDate;
    private Long   postID;
    private String content;

    public PostPOJO(Long postID, String userName, String content) {
        if (null == userName) { userName = "Anonymous"; }
        this.userName = userName;
        this.postID   = postID;
        this.content  = content;
        sendingDate   = new SimpleDateFormat("dd/MM/YY EEE hh:mm:ss").format(new Date());
    }

    public String getUserName() {
        return userName;
    }

    public PostPOJO setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getSendingDate() {
        return sendingDate;
    }

    public PostPOJO setSendingDate(String sendingDate) {
        this.sendingDate = sendingDate;
        return this;
    }

    public Long getPostID() {
        return postID;
    }

    public PostPOJO setPostID(Long postID) {
        this.postID = postID;
        return this;
    }

    public String getContent() {
        return content;
    }

    public PostPOJO setContent(String content) {
        this.content = content;
        return this;
    }
}
