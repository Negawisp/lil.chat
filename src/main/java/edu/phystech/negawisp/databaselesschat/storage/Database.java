package edu.phystech.negawisp.databaselesschat.storage;

import edu.phystech.negawisp.databaselesschat.storage.pojo.ThreadPOJO;

import java.util.HashMap;
import java.util.Map;

public class Database {
    private static Database singleton;

    private Map<String, ThreadPOJO> threads;


    private Database() {
        threads = new HashMap<>();
    }

    public static Database getSingleton () {
        if (null == singleton) {
            singleton = new Database();
        }
        return singleton;
    }

    public ThreadPOJO getCreateThread (String threadSubject) {
        ThreadPOJO thread = threads.get(threadSubject);
        if (null != thread) { return thread; }

        ThreadPOJO newThread = new ThreadPOJO();
        threads.put(threadSubject, newThread);
        return newThread;
    }
}
