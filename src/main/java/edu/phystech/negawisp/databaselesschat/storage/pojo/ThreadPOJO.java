package edu.phystech.negawisp.databaselesschat.storage.pojo;

import com.sun.scenario.effect.impl.prism.ps.PPSOneSamplerPeer;
import edu.phystech.negawisp.databaselesschat.dataasset.dto.PostDTO;

import java.util.*;


public class ThreadPOJO {
    private List<PostPOJO> replies;

    public ThreadPOJO() {
        replies = new ArrayList<>();
    }

    public PostPOJO addPost(long postID, PostDTO postDTO) {
        PostPOJO post = new PostPOJO( postID,
                                 postDTO.getUserName(),
                                 postDTO.getMessage() );
        replies.add(post);
        return post;
    }

    public boolean removePost (long postID) {
        for (PostPOJO post : replies) {
            if (post.getPostID() == postID) {
                replies.remove(post);
                return true;
            }
        }
        return false;
    }

    public String getThreadAsString() {
        StringBuilder sb = new StringBuilder();
        for (PostPOJO post : replies) {
            sb.append(post.getUserName()).append(' ')
              .append(post.getSendingDate()).append(' ')
              .append("No").append(post.getPostID()).append("\n\r\n\r")
              .append(post.getContent()).append("\n\r\n\r\n\r");
        }
        return sb.toString();
    }


}
