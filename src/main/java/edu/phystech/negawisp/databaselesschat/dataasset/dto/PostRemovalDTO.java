package edu.phystech.negawisp.databaselesschat.dataasset.dto;

/**
 * Ideas:
 * Comment with the reason of deletion
 */
public class PostRemovalDTO {
    private long   postID;

    public PostRemovalDTO() {}

    public long getPostID() {
        return postID;
    }

    public void setPostID(long postID) {
        this.postID = postID;
    }
}
