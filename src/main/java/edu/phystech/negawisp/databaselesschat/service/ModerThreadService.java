package edu.phystech.negawisp.databaselesschat.service;

import edu.phystech.negawisp.databaselesschat.dataasset.dto.PostRemovalDTO;
import edu.phystech.negawisp.databaselesschat.storage.Database;
import edu.phystech.negawisp.databaselesschat.storage.pojo.ThreadPOJO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
public class ModerThreadService extends ThreadService {

    public ModerThreadService() {
        super();
    }

    public ResponseEntity<String> deleteMessage (String threadName, PostRemovalDTO prDTO) {
        ThreadPOJO thread = database.getCreateThread(threadName);
        boolean success = thread.removePost(prDTO.getPostID());
        if (success) {
            return ResponseEntity.ok(thread.getThreadAsString());
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Post with such an ID does not exist.");
        }
    }
}
