package edu.phystech.negawisp.databaselesschat.service;

import edu.phystech.negawisp.databaselesschat.dataasset.dto.PostDTO;
import edu.phystech.negawisp.databaselesschat.storage.pojo.ThreadPOJO;
import edu.phystech.negawisp.databaselesschat.storage.Database;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ThreadService {
    protected Database database;

    public ThreadService() {
        database = Database.getSingleton();
    }


    public ResponseEntity addMessage (String threadSubject, long messageID, PostDTO postDTO) {
        database.getCreateThread(threadSubject).addPost(messageID, postDTO);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<String> getThreadAsString(String threadSubject) {
        ThreadPOJO chatPOJO = database.getCreateThread(threadSubject);
        if (null == chatPOJO) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Thread you were looking for does not exist.");
        }
        return ResponseEntity.ok(chatPOJO.getThreadAsString());
    }
}